/*//直接刷新页面即可
let oButtonreopen=document.querySelector('#again')
function Reopen(){
    location.reload();
}
oButtonreopen.addEventListener('click',Reopen)*/
//生成一个随机数字
//Math.random()  返回[0,1)之间的随机数
let toGuess=parseInt(Math.random()*100)  //在[0,100)之间随机数字
console.log("目标值:"+toGuess)
//事件驱动型
// 用户点击按钮开始猜数字

//事件处理逻辑

//事件源
let oButton=document.querySelector('#guessanniu')

//先得到用户的输入数字,用户在<input>输入的内容就是<input>的value属性
let oInput=document.querySelector('input')
let oCountSpan=document.querySelector('#count')
let oResultSpan=document.querySelector('#result')
let oHistory=document.querySelector('#history')

 let count=0  //记录猜的次数
function handle() {
  //进行 猜
    let guess=oInput.value   //此处得到的是字符串类型
    oInput.value=''   //清空输入框，可以连续输入
    guess=parseInt(guess)   //将字符串转换为整型
    //判断输入的是否为数字
    if (isNaN(guess)){
        alert('请输入数字！！！')
        return
    }
    console.log("用户输入了"+guess)
    //判断用户输入的数字和要猜的数字之间的关系，得到结果
    count+=1   //更新猜的次数

    //添加历史记录
    let html=`<div>第${count}次:${guess}</div>`
    oHistory.innerHTML +=html

    let result
    if (guess===toGuess){
        result="猜对了！"
        //停止使用输入框和 猜 的按钮
        oInput.disabled=true
        oButton.disabled=true
    }else if (guess<toGuess){
        result="猜小了!"
    }else if(guess>toGuess){
        result="猜大了!"
    }
    //更新显示结果，猜的次数以及结果
    oCountSpan.textContent=count

    oResultSpan.textContent=result
}

//事件类型click，进行绑定
oButton.addEventListener('click',handle)
//回车后也可以进行 猜 的流程
oInput.addEventListener('keydown',function (event) {
    //传入的参数事件
    console.log(event)
    //处理回车按钮
    if(event.key==='Enter'){
        handle()
    }
} )


//链式调用以及匿名函数  重新开始游戏按钮，也可以进行网页的刷新
document.querySelector('button#again').addEventListener('click',function () {
    //重新生成随机值
    let toGuess=parseInt(Math.random()*100)  //在[0,100)之间随机数字
    console.log("目标值:"+toGuess)
    //复原<input>和 猜 按钮
    oInput.disabled=false
    oButton.disabled=false
    //猜的次数复原
    count=0
    //显示结果复原
    oCountSpan.textContent=''

    oResultSpan.textContent=''
    //复原历史记录
    oHistory.innerHTML=''
})