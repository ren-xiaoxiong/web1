function 绿灯亮() {
    document.querySelector('.红灯').style.backgroundColor = 'rgba(255, 0, 0, .3)'
    document.querySelector('.黄灯').style.backgroundColor = 'rgba(255, 255, 0, .3)'
    document.querySelector('.绿灯').removeAttribute('style')  // 删除元素中的 style 属性
                                    // remove 删除
                                    // style 属性
                                    // 删除属性之后，保证去应用 demo2.css 写的样式
    document.querySelector('span').textContent = '绿'
}

function 红灯亮() {
    document.querySelector('.绿灯').style.backgroundColor = 'rgba(0, 255, 0,.3)'
    document.querySelector('.黄灯').style.backgroundColor = 'rgba(255, 255, 0, .3)'
    document.querySelector('.红灯').removeAttribute('style')
    document.querySelector('span').textContent = '红'
}

function 黄灯亮() {
    document.querySelector('.绿灯').style.backgroundColor = 'rgba(0, 255, 0, .3)'
    document.querySelector('.红灯').style.backgroundColor = 'rgba(255, 0, 0, .3)'
    document.querySelector('.黄灯').removeAttribute('style')
    document.querySelector('span').textContent = '黄'
}
//绿红黄轮换
function 绿灯亮一会儿切红灯() {
    绿灯亮()
    setTimeout(红灯亮一会儿切黄灯, 5000)
}

function 红灯亮一会儿切黄灯() {
    红灯亮()
    setTimeout(黄灯亮一会儿切绿灯, 6000)
}

function 黄灯亮一会儿切绿灯() {
    黄灯亮()
    setTimeout(绿灯亮一会儿切红灯, 2000)
}

/*绿灯亮一会儿切红灯()*/

/*
绿灯亮()

// window.setTimeout
// 设置超时 ...
// 实际上是一个定时器（指定 多长时间之后，运行我们提供的一个函数）
setTimeout(红灯亮, 5000)       // 第二个参数的单位是 毫秒
                              // 5000 毫秒之后，去调用我们的 红灯亮函数
                              // 注意：第一个参数是 红灯亮 而不是 红灯亮()
                              // 红灯亮 是 传入了一个函数
                              // 红灯亮()  先去调用红灯亮函数，然后把 红灯亮 函数的返回值传入
*/