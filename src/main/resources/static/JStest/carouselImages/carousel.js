//首先将所有图片地址保存到一个数组中
let 图片地址列表 = [
     "./dog1.jpg",
     "./dog2.jpg",
     "./dog3.jpg",
     "./dog4.jpg",
     "./dog5.jpg",
]
//定义一个下标记录当前显示的是那个图片
let currentIndex =0

function 显示图片(){
    //根据下标取出地址
    let source=图片地址列表[currentIndex]
    //更新ing标签的src属性
    //先找到<img>元素
    let oImg=document.querySelector('img')
    //更新src属性
    oImg.src=source;
    //让图片的下标指向下一个位置,如果到最后一张就回到第一张图片
    currentIndex +=1
    if (currentIndex===图片地址列表.length){
        currentIndex=0
    }
}

图片显示一段时间后切换下一个()     //一开始就进入轮播状态，不加本行九组要手动调用

function  图片显示一段时间后切换下一个(){
    显示图片()
    //每张图片停留5秒
    setTimeout(图片显示一段时间后切换下一个,5000)
}