package com.example.web1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FirstController {
    @GetMapping("/first-dynamic-resource")
    @ResponseBody
    public String demo(
            @RequestParam(value = "name",required = false) String name,
            @RequestParam(value = "age",required = false) String age
    ){
        System.out.println("请求参数 name =" + name);
        System.out.println("请求参数 age =" + age);

        return "<h1>我是动态资源</h1>";
    }


}