package com.example.web1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ControllerTest {
    @GetMapping("/urlencode-demo")
    @ResponseBody
    public String demo(@RequestParam("name") String name) {
        // 由于服务器已经帮我们做过 URL 解码了，所以我们一般不考虑这个问题
        System.out.println("name = |" + name + "|");

        return "成功";
    }

    @RequestMapping("/demo")    // 任意 HTTP 方法都支持
    @ResponseBody
    public String demo() {
        return "<h1>说明是 HTML</h1>";
    }
}
