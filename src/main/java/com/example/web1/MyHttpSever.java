package com.example.web1;

import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MyHttpSever {
    public static void main(String[] args) throws Exception {
        //使用 8080 端口
        try(ServerSocket serverSocket=new ServerSocket(8080)){
            while (true){
                Socket socket =serverSocket.accept(); //三次握手完成

                //读取用户的请求 : 这里不管用户的请求，采取同样的方式返回响应

                //发送响应
                //Content-Type:浏览器应该按照什么格式来看我们响应的资源(资源内容放在响应体中)
                String response ="HTTP/1.0 200 OK\r\n"
                        + "Sever:xiaoxiongSever\r\n"
                        + "ContentType:text/html; charset=utf-8\r\n"
                        + "\r\n"
                        + "<h1>你好，欢迎访问 MyHttpSever</h1>";    //响应体

                byte[] responseBytes = response.getBytes("UTF-8");
                OutputStream os = socket.getOutputStream();
                os.write(responseBytes);
                os.flush();

                //发送完成之后。直接关闭 TCP 连接(短连接的处理)
                socket.close();
            }
        }
    }
}
