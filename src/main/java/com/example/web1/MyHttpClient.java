package com.example.web1;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

// 只能进行一次请求-响应的 HTTP 客户端
public class MyHttpClient {
    public static void main(String[] args) throws Exception {
        // 主机                  127.0.0.1
        // 端口（进程）           8080
        // 资源路径              /hello.html
        try (Socket socket = new Socket("127.0.0.1", 8080)) {
            // 准备 HTTP 请求内容
            // 文本   String

            // 格式：请求行
            String requestLine = "GET /HTTP/hello.html HTTP/1.0\r\n";
            // 请求头：完全可以没有，但必须一个空行结尾
            String requestHeader1 = "Name: xaioxiong\r\n\r\n";  // 请求头中共有 1对 key-value
            String requestHeader2 = "Name: xiaoxiong\r\nAge: 1993\r\n\r\n";    // 请求头中共有 2对 key-value
            String requestHeader3 = "\r\n"; // 请求头中共有 0 对 key-value
            // 请求体，GET 是没有请求体

            // 最终的请求 —— 要发送给服务器的东西
            String request = requestLine + requestHeader3;

            // 发送服务器的过程
            byte[] requestBytes = request.getBytes(StandardCharsets.ISO_8859_1);   // 字符集编码

            // 发送（数据会经由 TCP、IP、以太网发送给服务器）
            OutputStream os = socket.getOutputStream();
            os.write(requestBytes);
            os.flush();

            // 请求既然已经发送，我们要做的就是等待响应
            InputStream is = socket.getInputStream();
            Scanner scanner = new Scanner(is, "UTF-8"); // 响应的前面字符集应该是 ISO-8859-1，后边是 UTF-8
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                System.out.println(line);
            }
        }
    }
}
